# Automatically build the slides on the reproducible paper
#
# Copyright (C) 2018-2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is part of Maneage. Maneage is free software: you can
# redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# Maneage is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. See <http://www.gnu.org/licenses/>.





# Clean all extra files
all: final
.PHONY: clean-latex clean all final
clean-latex:
	rm -f *.aux *.log *.nav *.out *.snm *.toc git-commit.tex
clean: clean-latex
	rm *.pdf





# PDF slides:
slides = slides-intro-short.pdf slides-intro.pdf
$(slides): %.pdf: %.tex tex/*.tex
        # We'll run pdflatex two times so the page numbers and
        # internal links also work.
	if [ -d .git ]; then v=$$(git describe --dirty --always --long); \
	else                 v=NO-GIT; fi; \
	echo "\newcommand{\gitcommit}{$$v}" > git-commit.tex
	pdflatex $*
#	pdflatex $*




# Ultimate aim of project.
final: $(slides)
