Slides to introduce Maneage (managing data lineage)
===================================================

Copyright (C) 2018-2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>\
See the end of the file for license conditions.

This is the LaTeX source of the slides presenting
[Maneage](https://gitlab.com/maneage/project). The most recent version
of the slides is available at
https://akhlaghi.org/pdf/maneage-introduction.pdf .

To build the slides, you need to have the Beamer and Tikz LaTeX
packages installed, then simply run this command.

```(shell)
$ make
```

Note that the `Makefile` will create a `git-commit.tex` file before
running LaTeX. So if you want to run LaTeX directly, make this file
manually, see the `Makefile` for the suggested command to generate it.





Copyright information
---------------------

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <https://www.gnu.org/licenses/>.